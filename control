Source: rawdog
Section: web
Priority: optional
Maintainer: Adam Sampson <ats@offog.org>
Build-Depends: debhelper (>= 11), dh-python, python (>= 2.7),
 python-feedparser (>= 5.1.2), python-tidylib
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/atsampson-guest/rawdog.git
Vcs-Browser: https://salsa.debian.org/atsampson-guest/rawdog
Homepage: http://offog.org/code/rawdog/

Package: rawdog
Provides: python-rawdoglib
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, python-feedparser (>= 5.1.2)
Recommends: python-tidylib
Description: RSS Aggregator Without Delusions Of Grandeur
 rawdog is a feed aggregator, capable of producing a personal "river of
 news" or a public "planet" page. It supports all common feed formats,
 including all versions of RSS and Atom. By default, it is run from
 cron, collects articles from a number of feeds, and generates a static
 HTML page listing the newest articles in date order. It supports
 per-feed customizable update times, and uses ETags, Last-Modified,
 gzip compression, and RFC3229+feed to minimize network bandwidth
 usage. Its behaviour is highly customisable using plugins written in
 Python.
